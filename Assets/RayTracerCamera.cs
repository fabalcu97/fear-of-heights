﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayTracerCamera : MonoBehaviour {

	public float reachDistance;
	private float timer;
	private GameObject get_Objetc;
	private GameObject pointer;
	private GameObject FPS;

	private Vector3 playerPosition;
	// Use this for initialization
	void Start () {
		//get_Objetc = GameObject.Find("Cube");
		//get_Objetc.GetComponent<Renderer>().material.color = Color.red;

		pointer = GameObject.Find("Pointer");
		pointer.GetComponent<Renderer>().material.color = Color.cyan;
		//playerPosition = this.transform.position;

		FPS = GameObject.Find("FPSController");
		

	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Ray touchingRay = new Ray(transform.position, Vector3.forward);
		get_Objetc = GameObject.Find("Level1");
		Debug.DrawRay(transform.position, Vector3.forward * reachDistance);
		//Debug.Log("antes del Drawray");
		timer += Time.deltaTime;
		pointer = GameObject.Find("Pointer");
		//Debug.Log(this.transform.position);
		//Debug.Log(touchingRay);

		//Debug.Log(timer);
		if(Physics.Raycast(touchingRay, out hit, reachDistance)){
			if(hit.collider.name == "Level1"){
				Debug.Log("Tocando algo");
				if((int)timer%5 == 0){
					Debug.Log("Tocando el Cubo");
					get_Objetc.GetComponent<Renderer>().material.color = Color.green;
					playerPosition = new Vector3(34.89f, -14.55f, 86.38f);
					FPS.transform.position = playerPosition;
					Debug.Log(FPS.transform.position);

				}
//				else{
//					get_Objetc.GetComponent<Renderer>().material.color = Color.red;
//				}
			}
		}
		//this.transform.position = playerPosition;
	}
}