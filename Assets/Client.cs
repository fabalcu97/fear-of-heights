﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Text;
using System.Net.Sockets;


public class Client : MonoBehaviour {
 
    private Socket _clientSocket = new Socket(AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp);
    private byte[] _recieveBuffer = new byte[8142];
	private String receivedMessage;
	private String[] options = {};

	public GameObject person;

	public TextMesh txt;
 
    private void Start()
    {
        try
        {
            _clientSocket.Connect(new IPEndPoint(IPAddress.Parse("192.168.1.178"), 9050));
			// _clientSocket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050));
        }
        catch(SocketException ex)
        {
            Debug.Log(ex.Message);
        }
 
        _clientSocket.BeginReceive(_recieveBuffer,0,_recieveBuffer.Length,SocketFlags.None,new AsyncCallback(ReceiveCallback),null);
		SendData(Encoding.ASCII.GetBytes("Hello!"));
 
    }
 
    private void ReceiveCallback(IAsyncResult AR)
    {
        //Check how much bytes are recieved and call EndRecieve to finalize handshake
        int recieved = _clientSocket.EndReceive(AR);
 
        if(recieved <= 0)
            return;
 
		receivedMessage = Encoding.ASCII.GetString(_recieveBuffer, 0, recieved);
        // Debug.Log(receivedMessage.Split('#'));
		// options = receivedMessage.Split('#');
        // options = receivedMessage;

        //Start receiving again
        _clientSocket.BeginReceive(_recieveBuffer,0,_recieveBuffer.Length,SocketFlags.None,new AsyncCallback(ReceiveCallback),null);
    }
 
    private void SendData(byte[] data)
    {
        SocketAsyncEventArgs socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data,0,data.Length);
        _clientSocket.SendAsync(socketAsyncData);
    }

	void Update () {
		txt.text = receivedMessage;
        Debug.Log(options);
        if (options.Length == 0) return;
        if (receivedMessage == "1") {
            person.transform.position = transform.position + Camera.main.transform.forward * Time.deltaTime * 45;
        }
	}
}